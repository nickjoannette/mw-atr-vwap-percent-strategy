package com.biiuse.motivewave;

import com.motivewave.platform.sdk.common.*;
import com.motivewave.platform.sdk.common.desc.*;
import com.motivewave.platform.sdk.order_mgmt.Order;
import com.motivewave.platform.sdk.order_mgmt.OrderContext;
import com.motivewave.platform.sdk.study.StudyHeader;
import com.motivewave.platform.sdk.common.*;
import com.motivewave.platform.sdk.common.desc.*;
import com.motivewave.platform.sdk.common.menu.MenuDescriptor;
import com.motivewave.platform.sdk.common.menu.MenuItem;
import com.motivewave.platform.sdk.common.menu.MenuSeparator;
import com.motivewave.platform.sdk.draw.Figure;
import com.motivewave.platform.sdk.study.RuntimeDescriptor;
import com.motivewave.platform.sdk.study.Study;
import com.motivewave.platform.sdk.study.StudyHeader;
import com.motivewave.platform.sdk.draw.Marker;
import java.awt.*;
import java.util.*;
import java.util.List;

/** Moving Average Cross Strategy. This is based of the SampleMACross study and adds the ability to trade. */
@StudyHeader(
        namespace="com.mycompany",
        id="ATR VWAP Percent Bands v1.3.1",
        name="ATR VWAP Percent Bands v1.3.1",
        desc="",
        menu="Fernando Torre-Sarlat",
        overlay = true,
        signals = true,
        strategy = true,
        autoEntry = true,
        manualEntry = false,
        supportsUnrealizedPL = true,
        supportsRealizedPL = true,

        supportsTotalPL = true,
        supportsUseAccountPosition = true
)

public class ATR_VWAP_Strategy extends Study
{
    enum TradeValues { PLACED_ENTRY_ORDER_THIS_BAR, PLACED_EXIT_ORDER_THIS_BAR }
    final String LONG = "long";
    final String SHORT = "short";
    final String ANY = "any";

    final String TRADE_CONSTRAINT = "tradeConstraint";
    final String MAX_TRADES_LONG_DIR = "maxTradesLongDir";
    final String MAX_TRADES_SHORT_DIR = "maxTradesShortDir";
    final String ORDER_GENERATION_METHOD = "orderGenerationMethod";
    final String POSITION_SIZING_MODE = "positionSizingMode";
    final String INCREMENTAL_VALUE_1 = "INCREMENTAL_VALUE_1";
    final String INCREMENTAL_VALUE_2 = "INCREMENTAL_VALUE_2";
    final String INCREMENTAL_VALUE_3 = "INCREMENTAL_VALUE_3";
    final String INCREMENTAL_VALUE_4 = "INCREMENTAL_VALUE_4";
    final String INCREMENTAL_VALUE_5 = "INCREMENTAL_VALUE_5";
    final String INCREMENTAL_TradeSize_1 = "INCREMENTAL_TradeSize_1";
    final String INCREMENTAL_TradeSize_2 = "INCREMENTAL_TradeSize_2";
    final String INCREMENTAL_TradeSize_3 = "INCREMENTAL_TradeSize_3";
    final String INCREMENTAL_TradeSize_4 = "INCREMENTAL_TradeSize_4";
    final String INCREMENTAL_TradeSize_5 = "INCREMENTAL_TradeSize_5";
    final String MINIMUM_INCREMENTAL_VALUE = "MINIMUM_INCREMENTAL_VALUE";
    final String MAX_RISK_PER_TRADE = "MAX_RISK_PER_TRADE";

    enum Values {VWAP, VWAPPlus, VWAPMinus, Trail, Loss, BuyEntry, SellEntry };

    final static String IND3 = "ind3", IND4 = "ind4";
    final static String ATR_PERIOD = "atrPeriod", ATR_FACTOR = "atrFactor";
    final String ORDER_TYPE = "orderType";
    final String USAGE = "usage";

    private final List<NVP> PositionSizingModes = List.of(
            new NVP("Incremental", "Incremental"),
            new NVP("Trading Options Trade Lots", "Trading Options Trade Lots"),
            new NVP("Max Risk Per Trade ($)", "Max Risk Per Trade ($)"));

    private static final List<NVP> OrderGeneration = List.of(
            new NVP("On Bar Close", "On Bar Close"),
            new NVP("Intrabar", "Intrabar"));

    private static final List<NVP> OrderType = List.of(
            new NVP("Market", "Market"),
            new NVP("Limit", "Limit"));

    private static final List<NVP> EntriesOrExits = List.of(
            new NVP("Entries Only", "Entries Only"),
            new NVP("Exits Only", "Exits Only"),
            new NVP("Both", "Both"));

    private static final List<NVP> TradeDirection = List.of(
            new NVP("Both", "Both"),
            new NVP("Long Only", "Long Only"),
            new NVP("Short Only", "Short Only"));


    @Override
    public void initialize(Defaults defaults) {

        SettingsDescriptor sd = new SettingsDescriptor();
        setSettingsDescriptor(sd);
        SettingTab tab=new SettingTab("General");
        sd.addTab(tab);

        SettingGroup vwap = new SettingGroup("VWAP");
        vwap.addRow(new InputDescriptor(Inputs.INPUT2, "VWAP Input", Enums.BarInput.TP));
        vwap.addRow(new BarSizeDescriptor(Inputs.BARSIZE, "VWAP Timeframe", BarSize.getBarSize(Enums.BarSizeType.LINEAR, 1440)));
        tab.addGroup(vwap);

        SettingGroup vwapPlusMinus = new SettingGroup("VWAP+-");
        vwapPlusMinus.addRow(new DoubleDescriptor(Inputs.PERIOD3, "Percent Above", 1.0, 0, 9999, 0.01));
        vwapPlusMinus.addRow(new DoubleDescriptor(Inputs.PERIOD4, "Percent Below", 1.0, 0, 9999, 0.01));

        tab.addGroup(vwapPlusMinus);
        SettingGroup atr_settings = new SettingGroup("ATR");
        atr_settings.addRow(new IntegerDescriptor(ATR_PERIOD, "ATR Period", 20, 0, 99999, 1));
        atr_settings.addRow(new DoubleDescriptor(ATR_FACTOR, "ATR Factor", 2.0, -99999, 999999, 0.01));
        tab.addGroup(atr_settings);

        tab=new SettingTab("Display");
        sd.addTab(tab);

        SettingGroup lines=new SettingGroup("VWAP Percent Plots");
        tab.addGroup(lines);
        lines.addRow(new PathDescriptor(Inputs.PATH2, "VWAP Plot", defaults.getBlueLine(), 1.0f, null, true, false, false));
        lines.addRow(new IndicatorDescriptor(Inputs.IND2, "Ind", defaults.getBlue(), Color.WHITE, false, false, true));

        lines.addRow(new PathDescriptor(Inputs.PATH3, "VWAP % Above Plot", defaults.getBlueLine(), 1.0f, null, true, false, false));
        lines.addRow(new IndicatorDescriptor(IND3, "Ind", defaults.getBlue(), Color.WHITE, false, false, true));

        lines.addRow(new PathDescriptor(Inputs.PATH4, "VWAP % Below Plot", defaults.getBlueLine(), 1.0f, null, true, false, false));
        lines.addRow(new IndicatorDescriptor(IND4, "Ind", defaults.getBlue(), Color.WHITE, false, false, true));

        SettingGroup lines2=new SettingGroup("ATR Plot");
        tab.addGroup(lines2);
        lines2.addRow(new PathDescriptor(Inputs.SIGNAL_PATH, "Trail", defaults.getGreenLine(), 1.0f, null, true, false, false));
        lines2.addRow(new IndicatorDescriptor(Inputs.SIGNAL_IND, "Ind", defaults.getGreen(), Color.WHITE, false, false, true));

        // Markers
        MarkerDescriptor BuyMarker = new MarkerDescriptor(Inputs.MARKER, "Buy Entry", Enums.MarkerType.LINE_ARROW,
                Enums.Size.MEDIUM, X11Colors.LIGHT_GREEN, X11Colors.LIGHT_GREEN, true, true);
        MarkerDescriptor SellMarker = new MarkerDescriptor(Inputs.MARKER2, "Sell Entry", Enums.MarkerType.LINE_ARROW,
                Enums.Size.MEDIUM, X11Colors.RED, X11Colors.RED, true, true);
        BuyMarker.setSupportsAdvanced(true);
        SellMarker.setSupportsAdvanced(true);

        SettingGroup markers = new SettingGroup("Markers");
        markers.addRow(BuyMarker);
        markers.addRow(SellMarker);
        tab.addGroup(markers);


        if (sd != null) {
            SettingTab tradeParams = new SettingTab("Strategy Options");

            sd.addTab(tradeParams);

            SettingGroup tradeparams = new SettingGroup("Parameters");
            tradeparams.addRow(new DiscreteDescriptor(USAGE, "Usage", "Entries Only", EntriesOrExits));
            tradeparams.addRow(new InputDescriptor(Inputs.PRICE_BAR, "Price", Enums.BarInput.CLOSE));
            tradeparams.addRow(new IntegerDescriptor(MAX_TRADES_LONG_DIR, "Max Trades in Long Direction", 2, 0, 99999, 1));
            tradeparams.addRow(new IntegerDescriptor(MAX_TRADES_SHORT_DIR, "Max Trades in Short Direction", 2, 0, 99999, 1));
            tradeparams.addRow(new DiscreteDescriptor(TRADE_CONSTRAINT, "Trade Direction", "Both", TradeDirection));
            tradeparams.addRow(new DiscreteDescriptor(POSITION_SIZING_MODE, "Position Sizing Mode", "Trading Options Trade Lots", PositionSizingModes));
            tradeparams.addRow(new DiscreteDescriptor(ORDER_GENERATION_METHOD, "Order Generation", "On Bar Close", OrderGeneration));
            tradeparams.addRow(new DiscreteDescriptor(ORDER_TYPE, "Order Type", "Market", OrderType));

            tradeParams.addGroup(tradeparams);
            SettingGroup max_risk = new SettingGroup("Max Risk Per Trade");
            max_risk.addRow(new DoubleDescriptor(MAX_RISK_PER_TRADE, "Max Risk Per Trade ($)", 50000.0,0.0,999999999.0,1.0));
            tradeParams.addGroup(max_risk);
            SettingGroup incremental_position_sizing = new SettingGroup("Incremental Position Sizing");
            incremental_position_sizing.addRow(

                    new DoubleDescriptor(MINIMUM_INCREMENTAL_VALUE, "Minimum price to enter", 10.0,0.0,999999999.0,1.0));

            incremental_position_sizing.addRow(
                    new LabelDescriptor("If close price <"),
                    new DoubleDescriptor(INCREMENTAL_VALUE_1, "", 50.0,0.0,999999999.0,1.0),
                    new IntegerDescriptor(INCREMENTAL_TradeSize_1, "then Trade Lots", 400,0,999999999,1));

            incremental_position_sizing.addRow(
                    new LabelDescriptor("If close price <"),
                    new DoubleDescriptor(INCREMENTAL_VALUE_2, "", 100.0,0.0,999999999.0,1.0),
                    new IntegerDescriptor(INCREMENTAL_TradeSize_2, "then Trade Lots", 300,0,999999999,1));

            incremental_position_sizing.addRow(
                    new LabelDescriptor("If close price <"),
                    new DoubleDescriptor(INCREMENTAL_VALUE_3, "", 200.0,0.0,999999999.0,1.0),
                    new IntegerDescriptor(INCREMENTAL_TradeSize_3, "then Trade Lots", 150,0,999999999,1));

            incremental_position_sizing.addRow(
                    new LabelDescriptor("If close price <"),
                    new DoubleDescriptor(INCREMENTAL_VALUE_4, "", 500.0,0.0,999999999.0,1.0),
                    new IntegerDescriptor(INCREMENTAL_TradeSize_4, "then Trade Lots", 50,0,999999999,1));

            incremental_position_sizing.addRow(
                    new IntegerDescriptor(INCREMENTAL_TradeSize_5, "else Trade Lots", 0,0,999999999,1));


            tradeParams.addGroup(incremental_position_sizing);


            List<NVP> tradeDirectionOptions = new ArrayList<NVP>();
            tradeDirectionOptions.add(new NVP("Long", LONG));
            tradeDirectionOptions.add(new NVP("Short", SHORT));
            tradeDirectionOptions.add(new NVP("Any", ANY));



            // Runtime Settings
            RuntimeDescriptor desc=new RuntimeDescriptor();
            setRuntimeDescriptor(desc);

            desc.setLabelSettings(Inputs.INPUT, Inputs.METHOD, Inputs.PERIOD, Inputs.INPUT2, Inputs.BARSIZE);
            desc.exportValue(new ValueDescriptor(Values.VWAP, "VWAP", new String[] { Inputs.INPUT2, Inputs.BARSIZE}));
            desc.exportValue(new ValueDescriptor(Values.VWAPPlus, "VWAP%+", new String[] { Inputs.PERIOD3}));
            desc.exportValue(new ValueDescriptor(Values.VWAPMinus, "VWAP%-", new String[] { Inputs.PERIOD3}));
            desc.exportValue(new ValueDescriptor(Values.Trail, "Trail", new String[] { Inputs.PERIOD3}));
            desc.exportValue(new ValueDescriptor(Values.Loss, "Loss", new String[] { Inputs.PERIOD3}));

            desc.declarePath(Values.VWAP, Inputs.PATH2);
            desc.declarePath(Values.VWAPPlus, Inputs.PATH3);
            desc.declarePath(Values.VWAPMinus, Inputs.PATH4);
            desc.declarePath(Values.Trail, Inputs.SIGNAL_PATH);

            desc.declareIndicator(Values.VWAP, Inputs.IND2);
            desc.declareIndicator(Values.VWAPPlus, IND3);
            desc.declareIndicator(Values.VWAPMinus, IND4);
            desc.declareIndicator(Values.Trail, Inputs.SIGNAL_IND);

            desc.setRangeKeys(Values.VWAP, Values.VWAPPlus, Values.VWAPMinus, Values.Trail, Values.Loss);

            desc.setLabelSettings(Inputs.INPUT2, Inputs.BARSIZE, Inputs.PERIOD3, Inputs.PERIOD4);
        }
    }


    protected int State = 1;
    protected int ordersInLongDirection = 0;
    protected int ordersInShortDirection = 0;
    protected int maxRiskLots=0;

    @Override
    protected void calculate(int index, DataContext ctx) {
        DataSeries series = ctx.getDataSeries();

        //calculate VWAP
        BarSize barSize = getSettings().getBarSize(Inputs.BARSIZE);
        DataSeries series2 = ctx.getDataSeries(barSize);
        Object input = getSettings().getInput(Inputs.INPUT2);
        if (series2.size() < 2) {
            //info("Series2 is null");
            return;
        }

        // Make sure the base bar size is smaller than the larger bar size
        if (series.getBarSize().getIntervalMinutes() >= series2.getBarSize().getIntervalMinutes()) {
            return;
        }

        // Find the first start of the day
        int start = -1;
        int day_ind = -1;
        boolean updates = getSettings().isBarUpdates();
        for (int i = 1; i < series.size(); i++) {
            if (!updates && !series.isBarComplete(i)) continue;
            long start_time = series.getStartTime(i);
            long prev_start_time = series.getStartTime(i - 1);
            // find the day bar where this starts
            for (int j = 0; j < series2.size(); j++) {
                long day_start = series2.getStartTime(j);
                long day_end = series2.getEndTime(j);
                if (day_start > start_time) continue;
                if (start_time < day_end && prev_start_time < day_start) {
                    day_ind = j;
                    start = i;
                    break;
                }
            }

            if (day_ind >= 0) break;
        }

        if (start < 0) {
            start = 0; // Just use the available data to calculate.
            //return;
        }

        int day_start_ind = start;
        for (int i = start; i < series.size(); i++) {
            if (!updates && !series.isBarComplete(i)) continue;
            // Find the starting bar of the day
            if (series.getStartTime(i) >= series2.getEndTime(day_ind)) {
                day_start_ind = i;
                day_ind++;
                if (day_ind >= series2.size()) break;

                series.setDouble(i, Values.VWAP, series.getDouble(i, input));
                //series.setComplete(i, series.isBarComplete(i));
                continue;
            }

            if (series.isComplete(i)) continue;

            // calc vwap
            double totalVolume = 0;
            double totalPrice = 0;
            for (int j = day_start_ind; j <= i; j++) {
                Double val = series.getDouble(j, input);
                if (val != null && !Double.isNaN(val)) {
                    totalPrice += val * series.getVolume(j);
                    totalVolume += series.getVolume(j);
                }
            }

            if (totalVolume == 0) return;
            series.setDouble(i, Values.VWAP, totalPrice / totalVolume);
            //series.setComplete(i, series.isBarComplete(i));
        }

        Double VWAP = series.getDouble(index, Values.VWAP);
        Double VWAPPlus = null;
        Double VWAPMinus = null;

        if (VWAP != null) {
            VWAPPlus = VWAP + getSettings().getDouble(Inputs.PERIOD3) * VWAP / 100d;
            VWAPMinus = VWAP - getSettings().getDouble(Inputs.PERIOD4) * VWAP / 100d;
        }

        if (VWAPPlus == null || VWAPMinus == null) return;
        series.setDouble(index, Values.VWAPPlus, VWAPPlus);
        series.setDouble(index, Values.VWAPMinus, VWAPMinus);
        // ATR Factor for atr trailing stop loss
        double ATRFactor = getSettings().getDouble(ATR_FACTOR);
        int ATRPeriod = getSettings().getInteger(ATR_PERIOD);
        Double satr = series.atr(ATRPeriod);
        if (satr.isNaN() || satr == null) return;
        double loss = ATRFactor * series.atr(ATRPeriod);
        series.setDouble(index, Values.Loss, loss);
        Double trail_1 = series.getDouble(index - 1, Values.Trail, Double.NaN);
        Double close = series.getDouble(index, Enums.BarInput.CLOSE, Double.NaN);

        switch (State) {
            case 1:
                if (close > trail_1) {
                    State = 1;
                    series.setDouble(index, Values.Trail, Math.max(trail_1, close - loss));
                } else {
                    State = -1;
                    series.setDouble(index, Values.Trail, close + loss);

                }
                break;
            case -1:
                if (close < trail_1) {
                    State = -1;
                    series.setDouble(index, Values.Trail, Math.min(trail_1, close + loss));
                } else {
                    State = 1;
                    series.setDouble(index, Values.Trail, close - loss);
                }
                break;

        }


        Object priceInput = getSettings().getInput(Inputs.PRICE_BAR);
        Double lowerBand = series.getDouble(index, Values.VWAPMinus, Double.NaN);
        Double upperBand = series.getDouble(index, Values.VWAPPlus, Double.NaN);

        boolean Condition2 = series.getDouble(index, priceInput, Double.NaN) < lowerBand;
        boolean Condition3 = series.getDouble(index, priceInput, Double.NaN) > upperBand;


        //Crosses above ATR
        boolean Condition4 = series.getDouble(index, Enums.BarInput.LOW, Double.NaN) < trail_1;
        int maxTradesLong = getSettings().getInteger(MAX_TRADES_LONG_DIR);
        int maxTradesShort = getSettings().getInteger(MAX_TRADES_SHORT_DIR);
        // LE
        boolean longsAllowed = !getSettings().getString(TRADE_CONSTRAINT).equals("Short Only");
        boolean shortsAllowed = !getSettings().getString(TRADE_CONSTRAINT).equals("Long Only");
        if (State == 1 && Condition2 && Condition4 && !series.isComplete(index, Values.BuyEntry) && longsAllowed) {

            MarkerInfo marker = getSettings().getMarker(Inputs.MARKER);
            Coordinate c = new Coordinate(series.getStartTime(index), series.getDouble(index, Enums.BarInput.LOW, Double.NaN));
            Marker m = new Marker(c, marker.getType(), marker.getSize(), Enums.Position.BOTTOM, marker.getFillColor(), marker.getOutlineColor());
            if (marker.isEnabled()) addFigure(m);

            series.setBoolean(index, Values.BuyEntry, true);
            series.setComplete(index, Values.BuyEntry, true);

        }

        // SE

        if (State == -1 && Condition3 && !series.isComplete(index, Values.SellEntry) && shortsAllowed) {
            MarkerInfo marker = getSettings().getMarker(Inputs.MARKER2);
            Coordinate c = new Coordinate(series.getStartTime(index), series.getDouble(index, Enums.BarInput.HIGH, Double.NaN));
            Marker m = new Marker(c, marker.getType(), marker.getSize(), Enums.Position.TOP, marker.getFillColor(), marker.getOutlineColor());
            if (marker.isEnabled()) addFigure(m);
            series.setBoolean(index, Values.SellEntry, true);
            series.setComplete(index, Values.SellEntry, true);
        }
        series.setComplete(index);
    }


    @Override
    public ATR_VWAP_Strategy clone() {
        ATR_VWAP_Strategy study = null;
        study = (ATR_VWAP_Strategy) super.clone();
        study.ordersInLongDirection = 0;
        study.ordersInShortDirection = 0;
        study.State = 1;
        study.maxRiskLots = 0;
        return study;
    }

    @Override
    public void onLoad(Defaults def) {
        super.onLoad(def);
        ordersInLongDirection = 0;
        ordersInShortDirection = 0;

        State = 1;
        maxRiskLots=0;
    }

    @Override
    public void onDeactivate(OrderContext ctx) {
        super.onDeactivate(ctx);
        ordersInLongDirection = 0;
        ordersInShortDirection = 0;

        State = 1;
        maxRiskLots = 0;
    }
    @Override
    public void onActivate(OrderContext ctx) {
        super.onActivate(ctx);
        ordersInLongDirection = 0;
        ordersInShortDirection = 0;

        State = 1;
        maxRiskLots = 0;
    }

    @Override
    public void onPositionClosed(OrderContext ctx) {
        ctx.cancelOrders();
        ordersInLongDirection = 0;
        ordersInShortDirection = 0;

        super.onPositionClosed(ctx);
    }

    @Override
    public void onOrderFilled(OrderContext ctx,Order order) {
        super.onOrderFilled(ctx,order);
        setEntryState(Enums.EntryState.OPEN);

        justFilled = true;
    }
    private boolean justFilled=false;


    @Override
    public void onBarUpdate(OrderContext ctx) {
        if (justFilled) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                info("E: " + e.toString());
            }
            justFilled=false;
            return;
        }
        if (!getSettings().getString(ORDER_GENERATION_METHOD).equals("Intrabar")) return;
        if ( ctx.getActiveOrders().size() == 0 &&
                (getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())) == 0) {
            ordersInShortDirection = 0;
            ordersInLongDirection = 0;
        }

        ordersInLongDirection = Math.max(0,ordersInLongDirection);
        ordersInShortDirection = Math.max(0,ordersInShortDirection);
        boolean noExits = getSettings().getString(USAGE).equals("Entries Only");
        boolean noEntries = getSettings().getString(USAGE).equals("Exits Only");
        boolean bothEntriesAndExits = getSettings().getString(USAGE).equals("Both");

        boolean longsAllowed = !getSettings().getString(TRADE_CONSTRAINT).equals("Short Only");
        boolean shortsAllowed = !getSettings().getString(TRADE_CONSTRAINT).equals("Long Only");
        boolean limitOrders = getSettings().getString(ORDER_TYPE).equals("Limit");
        DataSeries series = ctx.getDataContext().getDataSeries();
        int i = series.size() - 1;


        Double close = series.getDouble(i, Enums.BarInput.CLOSE,Double.NaN);
        if ((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()))==0)maxRiskLots=(int)(getSettings().getDouble(MAX_RISK_PER_TRADE)/close);
        int tradeSize = 0;

        switch (getSettings().getString(POSITION_SIZING_MODE)) {
            case "Max Risk Per Trade ($)":
                tradeSize = maxRiskLots;//(int)(getSettings().getDouble(MAX_RISK_PER_TRADE)/close);
// tradeSize = (int)(getSettings().getDouble(MAX_RISK_PER_TRADE)/close);
                break;

            case "Trading Options Trade Lots":
                tradeSize = getSettings().getTradeLots();
                break;

            case "Incremental":
                double incrementalValue1=getSettings().getDouble(INCREMENTAL_VALUE_1);
                double incrementalValue2=getSettings().getDouble(INCREMENTAL_VALUE_2);
                double incrementalValue3=getSettings().getDouble(INCREMENTAL_VALUE_3);
                double incrementalValue4=getSettings().getDouble(INCREMENTAL_VALUE_4);
                int incrementalTradeSize1=getSettings().getInteger(INCREMENTAL_TradeSize_1);
                int incrementalTradeSize2=getSettings().getInteger(INCREMENTAL_TradeSize_2);
                int incrementalTradeSize3=getSettings().getInteger(INCREMENTAL_TradeSize_3);
                int incrementalTradeSize4=getSettings().getInteger(INCREMENTAL_TradeSize_4);
                int incrementalTradeSize5=getSettings().getInteger(INCREMENTAL_TradeSize_5);
                double minimumIncrementalValue=getSettings().getDouble(MINIMUM_INCREMENTAL_VALUE);
                if (close > minimumIncrementalValue) {
                    if (close < incrementalValue1) tradeSize = incrementalTradeSize1;
                    else if (close < incrementalValue2) tradeSize = incrementalTradeSize2;
                    else if (close < incrementalValue3) tradeSize = incrementalTradeSize3;
                    else if (close < incrementalValue4) tradeSize = incrementalTradeSize4;
                    else tradeSize = incrementalTradeSize5;
                }

                break;
        }

        if (noEntries) tradeSize = Math.abs((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())));
        if (tradeSize== 0) return;

        // ATR Factor for atr trailing stop loss
        Object priceInput = getSettings().getInput(Inputs.PRICE_BAR);
        double ATRFactor = 2.0;
        int ATRPeriod = 14;
        double loss = ATRFactor * series.atr(ATRPeriod);

        Double lowerBand = series.getDouble(i, Values.VWAPMinus, Double.NaN);
        Double upperBand = series.getDouble(i, Values.VWAPPlus, Double.NaN);
        int Marketposition = (getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()));


        boolean Condition1 = tradeSize > 0;
        boolean Condition2 = series.getDouble(i, priceInput, Double.NaN) < lowerBand;
        boolean Condition3 = series.getDouble(i, priceInput, Double.NaN) > upperBand;

        //Crosses above ATR
        Double trail_1 = series.getDouble(i-1,Values.Trail,Double.NaN);
        boolean Condition4 = series.getDouble(i, Enums.BarInput.LOW, Double.NaN) < trail_1;

        int maxTradesLong = getSettings().getInteger(MAX_TRADES_LONG_DIR);
        int maxTradesShort = getSettings().getInteger(MAX_TRADES_SHORT_DIR);

        // LE
        boolean placedExitOrderThisBar = series.getBoolean(i,TradeValues.PLACED_EXIT_ORDER_THIS_BAR,false);
        boolean placedEntryOrderThisBar = series.getBoolean(i,TradeValues.PLACED_ENTRY_ORDER_THIS_BAR,false);

        if (ordersInLongDirection < maxTradesLong)
        if (((Marketposition == 0 && (noExits || bothEntriesAndExits)) || ((noEntries && Marketposition < 0)))) {
            if (State == 1 && Condition2 && Condition4 && !placedEntryOrderThisBar && longsAllowed && !(noEntries && Marketposition >= 0)) {

                ordersInLongDirection++;
                if (limitOrders) {
                    setEntryState(Enums.EntryState.WAITING_ENTRY);
                    series.setBoolean(i, TradeValues.PLACED_ENTRY_ORDER_THIS_BAR, true);
                    if (ctx.getInstrument().getAskPrice() >  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))) {
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.BUY, Enums.TIF.GTC, tradeSize,
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE, Float.NaN))));
                    } else ctx.buy(tradeSize);

                    return;

                }
                else ctx.buy(tradeSize);
                series.setBoolean(i, TradeValues.PLACED_ENTRY_ORDER_THIS_BAR, true);
                return;
            }

        }
        if ((Marketposition > 0 && !placedExitOrderThisBar && (!noExits || bothEntriesAndExits))) {

            if (State == -1 && Condition3 && ordersInLongDirection > 0) {
                ordersInLongDirection --;

                if (limitOrders) {
                    series.setBoolean(i, TradeValues.PLACED_EXIT_ORDER_THIS_BAR, true);

                    if (ctx.getInstrument().getBidPrice() <  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.SELL, Enums.TIF.GTC,tradeSize,
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))));
                    else ctx.sell(tradeSize);
                    return;

                }
                else
                    ctx.sell(tradeSize); // Sell
                series.setBoolean(i, TradeValues.PLACED_EXIT_ORDER_THIS_BAR, true);

                return;
            }
        }


        ////////////////LE Pyramid///////////////////////////
        if (Marketposition > 0 || (noEntries && Marketposition < 0)) {
            if (State == 1 && Condition2 && Condition4 && ordersInLongDirection < maxTradesLong && !placedEntryOrderThisBar && longsAllowed && !(noEntries && Marketposition >= 0)){
                ordersInLongDirection++;
                if (limitOrders) {
                    placedEntryOrderThisBar=true;
                    setEntryState(Enums.EntryState.WAITING_ENTRY);

                    series.setBoolean(i, TradeValues.PLACED_ENTRY_ORDER_THIS_BAR, true);
                    if (ctx.getInstrument().getAskPrice() > ctx.getInstrument().round(ctx.getInstrument().getLastPrice()))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.BUY, Enums.TIF.GTC,tradeSize,
                                ctx.getInstrument().round(ctx.getInstrument().getLastPrice())));
                    else ctx.buy(tradeSize);
                    return;

                }
                else
                    ctx.buy(tradeSize);
                series.setBoolean(i, TradeValues.PLACED_ENTRY_ORDER_THIS_BAR, true);

                return;

            }

            if (State == -1 && Condition3 && !placedExitOrderThisBar && ordersInLongDirection > 0 && !noExits && Marketposition > 0) {
                ordersInLongDirection --;

                if (limitOrders) {
                    series.setBoolean(i, TradeValues.PLACED_EXIT_ORDER_THIS_BAR, true);

                    if (ctx.getInstrument().getBidPrice() < ctx.getInstrument().round(ctx.getInstrument().getLastPrice()))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.SELL, Enums.TIF.GTC,(getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())),
                                ctx.getInstrument().round(ctx.getInstrument().getLastPrice())));
                    else ctx.sell(tradeSize);
                    return;

                }
                else
                    ctx.sell((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())));
                series.setBoolean(i, TradeValues.PLACED_EXIT_ORDER_THIS_BAR, true);

                return;
            }
        }

        //   if (noExits && Marketposition > 0) return;
        // SE
        if ((Marketposition == 0 && (noExits || bothEntriesAndExits))  || (noEntries && Marketposition > 0)) {
            if (State == -1 && Condition3 && ordersInShortDirection < maxTradesShort && !placedEntryOrderThisBar && shortsAllowed) {
                ordersInShortDirection ++ ;

                if (limitOrders) {                    setEntryState(Enums.EntryState.WAITING_ENTRY);

                    series.setBoolean(i, TradeValues.PLACED_ENTRY_ORDER_THIS_BAR, true);
                    placedEntryOrderThisBar=true;

                    if (ctx.getInstrument().getBidPrice() < ctx.getInstrument().round(ctx.getInstrument().getLastPrice()))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.SELL, Enums.TIF.GTC,tradeSize,
                                ctx.getInstrument().round(ctx.getInstrument().getLastPrice())));
                    else ctx.sell(tradeSize);
                    return;
                }
                else
                    ctx.sell(tradeSize);
                series.setBoolean(i, TradeValues.PLACED_ENTRY_ORDER_THIS_BAR, true);

                return;
            }
        }
        if (Marketposition < 0 && !noExits) {
            if (State == 1 && Condition2 && !placedExitOrderThisBar && ordersInShortDirection > 0) {
                ordersInShortDirection -- ;
                if (limitOrders) {
                    series.setBoolean(i, TradeValues.PLACED_EXIT_ORDER_THIS_BAR, true);
                    if (ctx.getInstrument().getAskPrice() > ctx.getInstrument().round(ctx.getInstrument().getLastPrice()))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.BUY, Enums.TIF.GTC,Math.abs((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()))),
                                ctx.getInstrument().round(ctx.getInstrument().getLastPrice())));
                    else ctx.buy(tradeSize);
                    return;

                }
                else
                    ctx.buy(Math.abs((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())))); // Cover
                series.setBoolean(i, TradeValues.PLACED_EXIT_ORDER_THIS_BAR, true);

                return;
            }
        }
        ////////////////SE Pyramid////////////////////////////
        if (Marketposition < 0 && shortsAllowed && !noEntries) {
            if (State == -1 && Condition3 && ordersInShortDirection < maxTradesShort && !placedEntryOrderThisBar){
                ordersInShortDirection++;

                if (limitOrders) {                    setEntryState(Enums.EntryState.WAITING_ENTRY);

                    series.setBoolean(i, TradeValues.PLACED_ENTRY_ORDER_THIS_BAR, true);
                    placedEntryOrderThisBar=true;

                    if (ctx.getInstrument().getBidPrice() < ctx.getInstrument().round(ctx.getInstrument().getLastPrice()))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.SELL, Enums.TIF.GTC,tradeSize,
                                ctx.getInstrument().round(ctx.getInstrument().getLastPrice())));
                    else ctx.sell(tradeSize);
                    return;

                }
                else
                    ctx.sell(tradeSize);
                series.setBoolean(i, TradeValues.PLACED_ENTRY_ORDER_THIS_BAR, true);

                return;
            }
            if (State == 1 && Condition2 && !placedExitOrderThisBar && ordersInShortDirection > 0 && !noExits) {
                --ordersInShortDirection;
                if (limitOrders) {
                    series.setBoolean(i, TradeValues.PLACED_EXIT_ORDER_THIS_BAR, true);
                    if (ctx.getInstrument().getAskPrice() > ctx.getInstrument().round(ctx.getInstrument().getLastPrice()))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.BUY, Enums.TIF.GTC,tradeSize,
                                ctx.getInstrument().round(ctx.getInstrument().getLastPrice())));
                    else ctx.buy(tradeSize);
                    return;
                }
                else
                    ctx.buy(tradeSize);
                series.setBoolean(i, TradeValues.PLACED_EXIT_ORDER_THIS_BAR, true);

                return;
            }
        }

    }

    @Override
    public void onBarClose(OrderContext ctx) {
        if (getSettings().getString(ORDER_GENERATION_METHOD).equals("Intrabar")) return;

        if (ctx.getActiveOrders().size() == 0 && (getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())) == 0) {
            ordersInShortDirection = 0;
            ordersInLongDirection = 0;
        }

        ordersInLongDirection = Math.max(0,ordersInLongDirection);
        ordersInShortDirection = Math.max(0,ordersInShortDirection);
        boolean noExits = getSettings().getString(USAGE).equals("Entries Only");
        boolean noEntries = getSettings().getString(USAGE).equals("Exits Only");
        boolean bothEntriesAndExits = getSettings().getString(USAGE).equals("Both");
        boolean limitOrders = getSettings().getString(ORDER_TYPE).equals("Limit");
        DataSeries series = ctx.getDataContext().getDataSeries();
        int i = series.size() - 1;

        Double close = series.getDouble(i, Enums.BarInput.CLOSE,Double.NaN);
        if ((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()))==0)maxRiskLots=(int)(getSettings().getDouble(MAX_RISK_PER_TRADE)/close);

        int tradeSize = 0;
        switch (getSettings().getString(POSITION_SIZING_MODE)) {
            case "Max Risk Per Trade ($)":
                tradeSize = maxRiskLots;//(int)(getSettings().getDouble(MAX_RISK_PER_TRADE)/close);
                break;

            case "Trading Options Trade Lots":
                tradeSize = getSettings().getTradeLots();
                break;

            case "Incremental":
                double incrementalValue1=getSettings().getDouble(INCREMENTAL_VALUE_1);
                double incrementalValue2=getSettings().getDouble(INCREMENTAL_VALUE_2);
                double incrementalValue3=getSettings().getDouble(INCREMENTAL_VALUE_3);
                double incrementalValue4=getSettings().getDouble(INCREMENTAL_VALUE_4);
                int incrementalTradeSize1=getSettings().getInteger(INCREMENTAL_TradeSize_1);
                int incrementalTradeSize2=getSettings().getInteger(INCREMENTAL_TradeSize_2);
                int incrementalTradeSize3=getSettings().getInteger(INCREMENTAL_TradeSize_3);
                int incrementalTradeSize4=getSettings().getInteger(INCREMENTAL_TradeSize_4);
                int incrementalTradeSize5=getSettings().getInteger(INCREMENTAL_TradeSize_5);
                double minimumIncrementalValue=getSettings().getDouble(MINIMUM_INCREMENTAL_VALUE);
                if (close > minimumIncrementalValue) {
                    if (close < incrementalValue1) tradeSize = incrementalTradeSize1;
                    else if (close < incrementalValue2) tradeSize = incrementalTradeSize2;
                    else if (close < incrementalValue3) tradeSize = incrementalTradeSize3;
                    else if (close < incrementalValue4) tradeSize = incrementalTradeSize4;
                    else tradeSize = incrementalTradeSize5;
                }

                break;
        }

        if (noEntries) tradeSize = Math.abs((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())));
        if (tradeSize== 0) return;



        // ATR Factor for atr trailing stop loss
        Object priceInput = Enums.BarInput.CLOSE;
        double ATRFactor = 2.0;
        int ATRPeriod = 14;
        double loss = ATRFactor * series.atr(ATRPeriod);

        Double lowerBand = series.getDouble(i, Values.VWAPMinus, Double.NaN);
        Double upperBand = series.getDouble(i, Values.VWAPPlus, Double.NaN);

        boolean Condition1 = tradeSize > 0;
        boolean Condition2 = series.getDouble(i, priceInput, Double.NaN) < lowerBand;
        boolean Condition3 = series.getDouble(i, priceInput, Double.NaN) > upperBand;

        //Crosses above ATR
        Double trail_1 = series.getDouble(i-1,Values.Trail,Double.NaN);
        boolean Condition4 = series.getDouble(i, Enums.BarInput.LOW, Double.NaN) < trail_1;

        boolean longsAllowed = !getSettings().getString(TRADE_CONSTRAINT).equals("Short Only");
        boolean shortsAllowed = !getSettings().getString(TRADE_CONSTRAINT).equals("Long Only");
        int Marketposition = (getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()));
        int maxTradesLong = getSettings().getInteger(MAX_TRADES_LONG_DIR);
        int maxTradesShort = getSettings().getInteger(MAX_TRADES_SHORT_DIR);

        // LE
        if (ordersInLongDirection < maxTradesLong)
        if (((Marketposition == 0 && (noExits || bothEntriesAndExits)) || ((noEntries && Marketposition < 0)))) {

            if (State == 1 && Condition2 && Condition4 && longsAllowed && !(noEntries && Marketposition >= 0)) {
                ordersInLongDirection++;
                if (limitOrders) {
                    if (ctx.getInstrument().getAskPrice() >  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.BUY, Enums.TIF.GTC,tradeSize,
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))));
                    else ctx.buy(tradeSize);
                    return;

                }
                else ctx.buy(tradeSize);
                return;
            }

        }

        if ((Marketposition > 0 && (!noExits || bothEntriesAndExits))) {
            if (State == -1 && Condition3 && ordersInLongDirection > 0) {
                ordersInLongDirection --;
                if (limitOrders) {
                    if (ctx.getInstrument().getBidPrice() <  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.SELL, Enums.TIF.GTC,(getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())),
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))));
                    else ctx.sell(tradeSize);
                    return;

                }
                else
                    ctx.sell((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()))); // Sell
                return;
            }
        }


        ////////////////LE Pyramid///////////////////////////
        if (Marketposition > 0 || (noEntries && Marketposition < 0)) {
            if (State == 1 && Condition2 && Condition4 && ordersInLongDirection < maxTradesLong && longsAllowed && !(noEntries && Marketposition >= 0)){
                ordersInLongDirection++;
                if (limitOrders) {
                    if (ctx.getInstrument().getAskPrice() >  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.BUY, Enums.TIF.GTC,tradeSize,
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))));
                    else ctx.buy(tradeSize);
                    return;

                }
                else
                    ctx.buy(tradeSize);
                return;

            }

            if (State == -1 && Condition3 && ordersInLongDirection > 0 && !noExits && Marketposition > 0) {
                ordersInLongDirection --;
                if (limitOrders) {
                    if (ctx.getInstrument().getBidPrice() <  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.SELL, Enums.TIF.GTC,(getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())),
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))));
                    else ctx.sell(tradeSize);
                    return;

                }
                else
                    ctx.sell((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())));
                return;
            }
        }


        // SE


        if ((Marketposition == 0 && (noExits || bothEntriesAndExits))  || (noEntries && Marketposition > 0)) {
            if (State == -1 && Condition3 && ordersInShortDirection < maxTradesShort && shortsAllowed) {
                ordersInShortDirection ++ ;
                if (limitOrders) {
                    if (ctx.getInstrument().getBidPrice() <  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.SELL, Enums.TIF.GTC,tradeSize,
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))));
                    else ctx.sell(tradeSize);
                    return;
                }
                else
                    ctx.sell(tradeSize);
                return;
            }
        }

        if (Marketposition < 0 && !noExits) {
            if (State == 1 && Condition2 && ordersInShortDirection > 0) {
                ordersInShortDirection -- ;
                if (limitOrders) {
                    if (ctx.getInstrument().getAskPrice() >  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.BUY, Enums.TIF.GTC,Math.abs((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()))),
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))));
                    else ctx.buy(tradeSize);
                    return;

                }
                else
                    ctx.buy(Math.abs((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument())))); // Cover
                return;
            }
        }
        ////////////////SE Pyramid////////////////////////////

        if (Marketposition < 0 && shortsAllowed && !noEntries) {
            if (State == -1 && Condition3 && ordersInShortDirection < maxTradesShort){
                ++ordersInShortDirection;
                if (limitOrders) {
                    if (ctx.getInstrument().getBidPrice() <  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.SELL, Enums.TIF.GTC, tradeSize,
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE, Float.NaN))));
                    else ctx.sell(tradeSize);
                    return;

                } else
                    ctx.sell(tradeSize);
                return;
            } else  if (State == 1 && Condition2 && ordersInShortDirection > 0 && !noExits) {
                --ordersInShortDirection;
                if (limitOrders) {
                    if (ctx.getInstrument().getAskPrice() >  ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN)))
                        ctx.submitOrders(ctx.createLimitOrder(Enums.OrderAction.BUY, Enums.TIF.GTC,Math.abs((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()))),
                                ctx.getInstrument().round(series.getFloat(i, Enums.BarInput.CLOSE,Float.NaN))));
                    else ctx.buy(tradeSize);
                    return;

                }
                else
                    ctx.buy(Math.abs((getSettings().isUseAccountPosition() ? ctx.getAccountPosition(ctx.getInstrument()) : ctx.getPosition(ctx.getInstrument()))));
                return;
            }
        }

    }

}
